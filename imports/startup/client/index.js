import { Mongo } from 'meteor/mongo';

import { BlazeLayout } from 'meteor/kadira:blaze-layout';

global.Posts = new Mongo.Collection(null);

BlazeLayout.setRoot('body');

Meteor.startup(function () {
  if (Meteor.isCordova) {

    //   note: some plugins exposes its namespace to the global scope, 
    // and others like cordova diagnostic adds it to 'small cased cordova' cordova.plugins obj
      console.log('cordova!!!');
      console.log('cordova obj', cordova)
      console.log('Cordova obj', Cordova)
      console.log('statusBar', StatusBar)
      console.log('admob', AdMob);
    if (AdMob) {
        console.log('admob!!!');
      AdMob.createBanner( {
        adId: 'ca-app-pub-5074567264699953/1618518228',
        position: AdMob.AD_POSITION.BOTTOM_CENTER,
        // isTesting: true,
        autoShow: true,
        success: function() {
          console.log("Received ad");
        },
        error: function() {
          console.log("No ad received");
        }
      });
    //   AdMob.prepareInterstitial( {
    //       adId:'ca-app-pub-5074567264699953/2447619828', 
    //       autoShow: false
    //     },
    //     function() {
    //       console.log("prepareInterstitial ad");
    //     },
    //     function() {
    //       console.log("No prepareInterstitial ad received");
    //     }
    //      );
    } else {
      console.log("No Admob");
    }
  } else {
    console.log("No Cordova ");
  }
});