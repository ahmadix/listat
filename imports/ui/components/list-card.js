import { Template } from 'meteor/templating';

import './list-card.html';


Template.listCard.helpers({
  listImage: function(){
    // console.log(this.page._embedded['wp:featuredmedia'][0].media_details.sizes.medium.source_url);
    // page = this.page;
    return this.list.data._embedded['wp:featuredmedia'][0].media_details.sizes.medium.source_url;
  }
})
