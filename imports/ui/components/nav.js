import { Template } from 'meteor/templating';

import './nav.html';


Template.nav.events({
    'click .hamburger': function (event, instance) {
        event.preventDefault();
        $('.ui.left.sidebar').sidebar('toggle');
    }
})