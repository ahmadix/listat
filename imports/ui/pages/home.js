import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';

import { ECow } from '/imports/libs/ecow/ecow';

import './home.html';



Template.home.onRendered(function(){
  let instance = this;
  $('.home-container').visibility({
    once: false,
    // update size when new content loads
    observeChanges: true,
    // load content on bottom edge visible
    onBottomVisible: function() {
      if (!instance.isLoadingInitialContent.get()) {

        // this block should be refactored to a separate function to be used elsewhere        
        instance.isLoadingMoreContent.set(true);
        console.log('loading new content');
        cow = new ECow('http://listat.ahmadix.webfactional.com/','wp-json/wp/v2/', 'WP-API:V2');
        cow.getPosts({_embed: 1, page: Session.get('postPagesCount') + 1}).then(function(result) {
          if (result.data.length === 0) {
            instance.isLoadingMoreContent.set(false);
            return
          }
          result.data.forEach(doc => {
            Posts.insert({_id: String(doc.id), data: doc});
          });
          instance.isLoadingMoreContent.set(false);        
          Session.set('postPagesCount', Session.get('postPagesCount') + 1)  
          console.log('pages count', Session.get('postPagesCount'))      
          // console.log('store', Posts.find().fetch());
        }).catch((error) => {
          throw new Meteor.Error('500', `${error.message}`);
        });
      }
    }
  });
});

Template.home.onCreated(function(){
  // this.lists = new ReactiveVar([]);
  this.isLoadingInitialContent = new ReactiveVar(false);
  this.isLoadingMoreContent = new ReactiveVar(false);

  // notice that this session variable is sticky between refreshs due to hotcode push, so we need to invalidate it
  Session.setDefault('postPagesCount', 0);
  console.log('get session pagescount', Session.get('postPagesCount'));
  let instance = this;
  this.autorun(function(){
    if (!Posts.findOne()) {

      // this block should be refactored to a separate function to be used elsewhere
      instance.isLoadingInitialContent.set(true);
      cow = new ECow('http://listat.ahmadix.webfactional.com/','wp-json/wp/v2/', 'WP-API:V2');
      cow.getPosts({_embed: 1}).then(function(result) {
        if (result.data.length === 0) {
          instance.isLoadingInitialContent.set(false);
          return
        }
        result.data.forEach(doc => {
          Posts.insert({_id: String(doc.id), data: doc});
        });
        instance.isLoadingInitialContent.set(false);        
        // instance.postPagesCount.set(instance.postPagesCount.get() + 1);
        Session.set('postPagesCount', Session.get('postPagesCount') + 1)  
        console.log('pages count', Session.get('postPagesCount'))      
        // console.log('store', Posts.find().fetch());
      }).catch((error) => {
        throw new Meteor.Error('500', `${error.message}`);
      });

    } else {
      // check new lists and show bar with number of new lists and add them to Posts onclick
      // instance.isLoadingInitialContent = new ReactiveVar(true);
      console.log('content already loaded')
    }
    
  })
});

Template.home.helpers({
  'lists': function() {
    console.log('retrieve', Posts.find().fetch());
    return Posts.find({}, {sort: {'data.date': -1}}).fetch();
  },
  'isLoadingInitialContent': function() {
    console.log('isloading', Template.instance().isLoadingInitialContent.get());
    return Template.instance().isLoadingInitialContent.get();
  },
  'isLoadingMoreContent': function() {
    console.log('isloading More', Template.instance().isLoadingMoreContent.get());
    return Template.instance().isLoadingMoreContent.get();
  }
});
