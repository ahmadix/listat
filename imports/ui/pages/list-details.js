import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import { FlowRouter } from 'meteor/kadira:flow-router';

import { ECow } from '/imports/libs/ecow/ecow';

import './list-details.html';



Template.listDetails.onRendered(function () {

  console.log('onRendered');
  let instance = this;
  this.autorun(function () {
    if (!!instance.find('iframe')) {
      console.log('iframe rendered');
      let widgetIframe = document.querySelector('iframe');
      let widget = SC.Widget(widgetIframe);
      widget.bind(SC.Widget.Events.READY, function () {
        console.log('ready ready!');

        // widget.isPaused(function(data){
        //   console.log(data);
        // });
        // Meteor.setTimeout(function() {
        //   widget.play()
        // }, 0)

        widget.play()
        // widget.pause()
      });

      widget.bind(SC.Widget.Events.PLAY, function () {

        // get information about currently playing sound
        widget.getCurrentSound(function (currentSound) {
          // debugger;
          currentTrack = currentSound;
          console.log(currentSound);
          // console.log('sound ' + currentSound.title + 'began to play');

          //create the notification
          MusicControls.create({
            track: currentSound.title,		// optional, default : '' 
            artist: currentSound.user.full_name,						// optional, default : '' 
            cover: currentSound.user.avatar_url,		// optional, default : nothing 
            isPlaying: true,							// optional, default : true 
            dismissable: false,							// optional, default : false 

            // hide previous/next/close buttons: 
            hasPrev: true,		// show previous button, optional, default: true 
            hasNext: true,		// show next button, optional, default: true 
            hasClose: false,		// show close button, optional, default: false 

            // Android only, optional 
            // text displayed in the status bar when the notification (and the ticker) are updated 
            ticker: `Now playing ${currentSound.title}`
          }, function () {
            console.log('notif sucssess');
          }, function () {
            console.log('notif error');
          });
        });

        function notifEvents(action) {
          switch (action) {
            case 'music-controls-next':
              widget.next();
              break;
            case 'music-controls-previous':
              widget.prev();
              break;
            case 'music-controls-pause':
              widget.pause();
              break;
            case 'music-controls-play':
              widget.play();
              break;
            case 'music-controls-destroy':
              // Do something
              break;

            // Headset events (Android only)
            case 'music-controls-media-button':
              // Do something
              break;
            case 'music-controls-headset-unplugged':
              widget.pause();
              break;
            case 'music-controls-headset-plugged':
              // Do something
              break;
            default:
              break;
          }
        }

        // Register callback
        MusicControls.subscribe(notifEvents);

        // Start listening for events
        // The plugin will run the events function each time an event is fired
        MusicControls.listen();

        widget.bind(SC.Widget.Events.PAUSE, function () {
          MusicControls.updateIsPlaying(false)
        });

      });
    }
  });

  Template.listDetails.onDestroyed(function(){
    MusicControls.destroy();
  })

  // good for animating elements insert and removal from DOM
  // self.find('.player')._uihooks = {
  //   insertElement: function (node, next) {
  //     console.log('node', node);
  //     $(node)
  //       .hide()
  //       .insertBefore(next)
  //       .fadeIn();

  //     let widgetIframe = document.querySelector('iframe');
  //     let widget = SC.Widget(widgetIframe);
  //     widget.bind(SC.Widget.Events.READY, function () {
  //       console.log('ready ready!')
  //       widget.bind(SC.Widget.Events.PLAY, function () {
  //         // get information about currently playing sound
  //         widget.getCurrentSound(function (currentSound) {
  //           console.log(currentSound);
  //           console.log('sound ' + currentSound.title + 'began to play');
  //         });

  //         widget.getSounds(function (sounds) {
  //           console.log(sounds);
  //           // console.log('sound ' + sounds + 'began to play');
  //         });
  //       });
  //       // get current level of volume
  //       widget.getVolume(function (volume) {
  //         console.log('current volume value is ' + volume);
  //       });
  //       // set new volume level
  //       widget.setVolume(50);
  //       // get the value of the current position
  //     });
  //   },
  //   removeElement: function (node) {
  //     console.log(node)
  //     $(node).fadeOut(function () {
  //       self.remove();
  //     });
  //   }
  // }
});


Template.listDetails.onCreated(function () {
  this.post = new ReactiveVar([]);

  let instance = this;
  this.autorun(function () {
    let p = Posts.findOne({ _id: FlowRouter.getParam('postId') });
    if (!!p) {
      instance.post.set(p.data);
    } else {
      cow = new ECow('http://listat.ahmadix.webfactional.com/', 'wp-json/wp/v2/', 'WP-API:V2');
      cow.getPosts(FlowRouter.getParam('postId')).then(function (result) {
        instance.post.set(result.data);
      }).catch((error) => {
        throw new Meteor.Error('500', `${error.message}`);
      });
    }
  })
});

Template.listDetails.helpers({
  'post': function () {
    console.log(Template.instance().post.get())
    return Template.instance().post.get();
  },
  postImage: function () {
    // console.log(this.post._embedded['wp:featuredmedia'].media_details.sizes.medium.source_url);
    // post = this.post;
    return Template.instance().post.get()._embedded['wp:featuredmedia'][0].media_details.sizes.medium.source_url;
  }
});
