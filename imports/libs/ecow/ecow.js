import { HTTP } from 'meteor/http'

export class ECow {
	constructor(domain, apiBase, apiUsed) {
		// domain ending in forward slash
		this.domain = domain;
		this.apiBase = apiBase;
		this.apiUsed = apiUsed;
	}

	_buildURL(endpoint, queryParams) {
		// refere here for list of 'filter[parms]' supported
		// https://codex.wordpress.org/Class_Reference/WP_Query
		// and here for WP-API builtin supported params
		// http://v2.wp-api.org/reference/pages/
		// good article
		// https://1fix.io/blog/2015/09/29/wp-api-v2-angularjs-theme/
		
		function buildParams(params) {
			if(params instanceof Object) {
				let result = '?';
	 			Object.keys(params)
					.forEach(function(key){
						// console.log(key);
						result += key + '=' + params[key] + '&'
					});
			  return result;
			} else {
				// console.log(params);
				return '/'+params;
			}
		}

		let url = this.domain + this.apiBase + endpoint + buildParams(queryParams);
		// console.log(url);
		return url;
	}

	_performRequest(endpoint = '', queryParams = '') {

		return new Promise((resolve, reject) => {
		  HTTP.call('GET', this._buildURL(endpoint, queryParams), {},
			(error, result) => {
		    if (error) {
		      reject(error);
		    } else {
		      resolve(result);
		    }
		  });
		});
	};

	getPosts(queryParams) {
		return this._performRequest('posts', queryParams);
	}

	getMedia(queryParams) {
		return this._performRequest('media', queryParams);
	}

	getCategories(queryParams) {
		return this._performRequest('categories', queryParams);
	}

	getPages(queryParams) {
		return this._performRequest('pages', queryParams);
	}

	discoverAPI() {
		return this._performRequest();
	}

}
