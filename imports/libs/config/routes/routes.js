import { Session } from 'meteor/session';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '/imports/ui/layouts/main-layout.js';
import '/imports/ui/components/nav.js';
import '/imports/ui/components/list-card.js';
import '/imports/ui/pages/home.js';
import '/imports/ui/pages/list-details.js';
// import '/imports/ui/pages/chapter-page.js';

Session.setDefault('openedListsCount', 0);

FlowRouter.route('/', {
	name: 'home',
	action: function (params, queryParams) {
		BlazeLayout.render('mainLayout', { nav: 'nav', content: 'home' });
	}
});


FlowRouter.route('/list-details/:postId', {
	name: 'listDetails',
	action: function (params, queryParams) {
		BlazeLayout.render('mainLayout', { nav: 'nav', content: 'listDetails' });
		if (parseInt(Session.get('openedListsCount')) > 3) {
			console.log('more than 3', parseInt(Session.get('openedListsCount')));
			Meteor.startup(function () {
				if (Meteor.isCordova) {
					if (AdMob) {
						console.log('11admob!!!');
						AdMob.prepareInterstitial({
							adId: 'ca-app-pub-5074567264699953/2447619828',
							autoShow: false
						},
							function () {
								console.log("in playlist prepareInterstitial ad");
							},
							function () {
								console.log("in playlist No prepareInterstitial ad received");
							});
					} else {
						console.log("No Admob");
					}
				} else {
					console.log("No Cordova ");
				}
			});
		}
		Session.set('openedListsCount', Session.get('openedListsCount') + 1);
		console.log('opened count', Session.get('openedListsCount'))
	},
	triggersExit: [function () {
		Meteor.startup(function () {
			if (Meteor.isCordova) {
				if (AdMob) {
					if (parseInt(Session.get('openedListsCount')) > 3) {
						AdMob.showInterstitial();
					}
				}
			}
		})

	}]
});

