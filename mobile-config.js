App.info({
  name: 'Listat',
  description: 'Power of the people',
  author: 'Ahmad',
  email: 'example@email.com',
  website: 'https://github.com/',
  version: '0.0.1',
});

App.setPreference('StatusBarOverlaysWebView', 'false');
App.setPreference('StatusBarBackgroundColor', '#6435c9');
App.setPreference('Orientation', 'portrait');

App.accessRule('http://*', {type: 'intent'});
App.accessRule('https://*', {type: 'intent'});

// my solve open links in external device browser
// https://forums.meteor.com/t/open-links-outside-the-app/4538/26
// App.accessRule('*', { type: 'navigation' })
